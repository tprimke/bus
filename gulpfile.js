var gulp   = require('gulp'),
    babel  = require('gulp-babel'),
    del    = require('del'),
    rename = require('gulp-rename'),
    mocha  = require('gulp-mocha');

gulp.task( 'clean', (cb) => {
  del([ 'build/*', 'dist/*' ])
    .then( () => { cb(); } );
});

var renameJS = (path) => path.extname = '.js';

var compile = (pth, dest) => {
  return gulp.src(pth)
             .pipe( babel({ presets: ['es2015']}) )
             .pipe( rename( renameJS ) )
             .pipe( gulp.dest('build/' + dest) );
};

gulp.task( 'build-es6', ['clean'], () => compile('src/**/*.es6', 'src') );

gulp.task( 'dist', ['build-es6'], () => {
  return gulp.src('build/src/bus.js')
             .pipe( gulp.dest('dist') );
});

gulp.task( 'build-tests', ['clean'], () => compile('test/**/*.es6', 'test') );

gulp.task( 'test', ['build-es6', 'build-tests'], () => {
  return gulp.src(['build/test/**/test-*.js'], {read: false})
             .pipe( mocha({ reporter: 'spec'}) );
});

