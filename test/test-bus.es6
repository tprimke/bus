var should = require('should'),
    sinon  = require('sinon'),
    Bus    = require('../src/bus');

describe('Bus', () => {

  beforeEach( () => {
    Bus.clear();
  });

  it('more than one handler for a message', () => {
    let handler1 = sinon.spy(),
        handler2 = sinon.spy();
    Bus.on('msg', handler1);
    Bus.on('msg', handler2);

    Bus.send('msg', 1, 2, 3);

    handler1.calledOnce.should.be.true();
    handler1.calledWith(1, 2, 3).should.be.true();
    handler2.calledOnce.should.be.true();
    handler2.calledWith(1, 2, 3).should.be.true();
  });

  it('send and handle a message', () => {
    let handler = sinon.spy();
    Bus.on('msg', handler);

    Bus.send('msg', 1, 2, 3);
    handler.calledOnce.should.be.true();
    handler.calledWith(1, 2, 3).should.be.true();

    Bus.send('yada', 3);
    handler.calledOnce.should.be.true();
  });

  it('Bus should be a singleton', () => {
    let handler = sinon.spy();

    (function() {
      var b = require('../src/bus');
      b.on('msg', handler);
    })();

    Bus.send('msg', 1, 2, 3);
    handler.calledOnce.should.be.true();
    handler.calledWith(1, 2, 3).should.be.true();
  });

  it('Bus should provide only communication functions', function() {
    let functions = ['send', 'on', 'websocket', 'clear'];

    for ( let i in Bus )
      if ( Bus.hasOwnProperty(i) ) {
        if ( functions.indexOf(i) !== -1 )
          continue;

        (false).should.be.true();
      }
  });
  
  it('removing only selected handlers', function() {
    let h1 = sinon.spy(),
        h2 = sinon.spy(),
        h3 = sinon.spy();

    Bus.on('msg', h1);
    Bus.on('msg', h2);
    Bus.on('msg', h3);

    Bus.clear(h2, h3);

    Bus.send('msg');

    h1.calledOnce.should.be.true();
    h2.callCount.should.eql(0);
    h3.callCount.should.eql(0);
  });

  let checkVal = (value, type, val) => {
    value.should.be.an.Object();
    value.should.have.property('type');
    value.type.should.eql(type);
    if ( type === 'value' )
      value.value.should.eql(val);
    else {
      value.value.forEach( (e,idx) => {
        e.should.eql(val[idx]);
      });
      val.forEach( (e,idx) => {
        value.value[idx].should.eql(e);
      });
    }
  };

  let checkValue = (value, val) => checkVal(value, 'value', val);

  let checkArray = (value, val) => checkVal(value, 'array', val);

  describe( 'synchronous handlers', function() {

    beforeEach( function() {
      Bus.clear();
    });
    
    
    it('synchronous return value', function() {
      Bus.on('msg', () => 'ok' );

      let val = Bus.send('msg');

      checkValue(val, 'ok');
    });

    it('return value with promise', function(cb) {
      Bus.on('msg', () => 'ok' );

      Bus.send.promise('msg')
         .then( (val) => {
           checkValue(val, 'ok');
           cb();
         })
         .catch( () => {
           false.should.be.true();
           cb();
         });
    });

  }); // synchronous handlers

  describe( 'asynchronous handlers', function() {

    beforeEach( function() {
      Bus.clear();
    });
    
    
    it('handler returns a value', function(cb) {
      let handler = sinon.stub();
      handler.returns('success');

      Bus.on.promise('msg', handler);

      Bus.send.promise('msg')
         .then( (val) => {
           checkValue(val, 'success');
           cb();
         })
         .catch( () => {
           (false).should.be.true();
           cb();
         });
    });

    it('handler returns a promise', function(done) {
      let handler = sinon.stub();
      handler.returns( new Promise( (resolve, reject) => {
        resolve('success');
      }));

      Bus.on.promise('msg', handler);

      Bus.send.promise('msg')
         .then( (val) => {
           checkValue(val, 'success');
           done();
         })
         .catch( () => {
           (false).should.be.true();
           done();
         });
    });

    it('throwing exception in handler', function(done) {
      let handler = sinon.stub();
      handler.throws();

      Bus.on.promise('msg', handler);

      Bus.send.promise('msg')
         .then( () => {
           (false).should.be.true();
           done();
         })
         .catch( () => {
           (true).should.be.true();
           done();
         });
    });

    it('synchronous send should throw', function(done) {
      Bus.on.promise('msg', () => 'ok');

      (() => {
        Bus.send('msg');
      }).should.throw();

      done();
    });

    /* NOT SURE ABOUT THAT
    it('one failing handler causes the send to fail', function() {
      let handler1 = sinon.stub();
      handler1.returns(1);
      let handler2 = sinon.stub();
      handler2.returns( new Promise( (fulfill, reject) => {
        setTimeout( () => reject(), 100 );
      }));

      Bus.on('msg', handler1);
      Bus.on('msg', handler2);

      Bus.send('msg')
         .then( () => {
           (false).should.be.true();
         })
         .catch( () => {
           (true).should.be.true();
         });
    });
    */

  }); // asynchronous handlers

  describe( 'only one type of handlers is allowed', function() {

    beforeEach( function() {
      Bus.clear();
    });
    
    
    it('synchronous handlers', function() {
      (() => {
        Bus.on('msg', () => 'ok');
        Bus.on.promise('msg', () => 'ok');
      }).should.throw();
    });

    it('asynchronous handlers', function() {
      (() => {
        Bus.on.promise('msg', () => 'ok');
        Bus.on('msg', () => 'ok');
      }).should.throw();
    });

  }); // only one type of handlers is allowed
  
  describe( 'many handlers', function() {
    
    it('promise fulfilled after all asynchronous handlers are done', function(done) {
      let handler1 = sinon.stub();
      handler1.returns(1);
      let handler2 = sinon.stub();
      handler2.returns( new Promise( (fulfill, reject) => {
        setTimeout( () => { fulfill(2); }, 100 );
      }));

      Bus.on.promise('msg', handler1);
      Bus.on.promise('msg', handler2);

      Bus.send.promise('msg')
         .then( (results) => {
           checkArray(results, [1, 2]);
           done();
         })
         .catch( () => {
           (false).should.be.true();
           done();
         });
    });

    it('synchronous');

    // asynchronous:
    //   - all should be fullfilled, before the promise is fullfilled
    // 
    // synchronous:
    //   - all should be fine, before the final value is returned
  }); // many handlers  

});
