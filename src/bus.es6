module.exports = (function(){

  // ----------------------------------------------------------------
  // Private properties and functions
  
  // returns empty store - for initialisation purposes
  let emptyStore = () => [];

  // collection of records (message, handler, type)
  let store = emptyStore();

  // returns a new collection record - for adding handlers
  let newRecord = (message, handler, type = 'sync') => {
    return {
      message, handler, type
    };
  };

  // returns all the records for the given message
  let findHandlersM = (msg) => {
    return store.filter( (r) => r.message === msg );
  };

  // returns true, iff all the handlers for the given message
  // are of the given type
  // when there are no handlers, returns true
  let allType = (msg, type) => {
    let recs = store.filter( (r) => r.message === msg );
    if ( recs.length === 0 )
      return true;
    return recs.every( (r) => r.type === type );
  };

  let addSyncHandler = (msg, handler) => {
    store.push( newRecord(msg, handler) );
  };

  let addAsyncHandler = (msg, handler) => {
    store.push( newRecord(msg, handler, 'async') );
  };

  // two following functions are used to create values
  // that is returned from the send functions.

  let makeValue = (value) => {
    return {
      type: 'value',
      value
    };
  };

  let makeArray = (value) => {
    return {
      type: 'array',
      value
    };
  };
  
  // ----------------------------------------------------------------
  // Public functions

  let send = (message, ...args) => {

    // find the handlers for the message
    let handlers = findHandlersM(message);
    if ( handlers.length === 0 )
      return;

    // check asynchronous handlers
    if ( handlers.some( (r) => r.type === 'async' ) === true )
      throw new Error('Cannot synchronously send message to asynchronous handler.');

    // collect the returned values and return them
    let values = handlers.map( (r) => r.handler(...args) );
    if ( values.length === 1 )
      return makeValue(values[0]);
  };

  send.promise = (msg, ...args) => {
    let handlers = findHandlersM(msg);
    if ( handlers.length === 0 ) {
      return;
    }
    
    // check the handler type
    if ( handlers[0].type === 'sync' ) {
      let vals = send(msg, ...args);
      return Promise.resolve(vals);
    }

    // handle asynchronous handlers
                   // collect the values returned by handlers
    let promises = handlers.map( (r) => {
                                   try {
                                     let val = r.handler(...args);
                                     return val;
                                   }
                                   catch (e) {
                                     return Promise.reject(e);
                                   }
                                 })
                   // convert the values to promises, when needed
                           .map( (p) => {
                             if ( p instanceof Promise )
                               return p;
                             return Promise.resolve(p);
                           });
      // wait for all the promises to be resolved
    return new Promise( (resolve, reject) => {
      Promise.all(promises)
             .then( (values) => {
               if ( values.length === 1 )
                 resolve( makeValue(values[0]) );
               else {
                 resolve( makeArray(values) );
               }
             })
             .catch( (err) => {
               reject(err);
             });
    });
  };

  let on = (message, handler) => {
    if ( ! allType(message, 'sync') )
      throw new Error(`The ${message} handler should be asynchronous.`);
    addSyncHandler(message, handler);
  };

  on.promise = (message, handler) => {
    if ( ! allType(message, 'async') )
      throw new Error(`The ${message} handler should be synchronous.`);
    addAsyncHandler(message, handler);
  };

  let clear = (...handlers) => {
    // when no handlers are given, simply delete all handlers
    if ( handlers.length === 0 ) {
      store = emptyStore();
      return;
    }

    // otherwise, delete only the given handlers
    store = store.reduce( (acc, elm) => {
      if ( handlers.indexOf(elm.handler) === -1 )
        acc.push(elm);
      return acc;
    }, []);
  };

  // ----------------------------------------------------------------
  // Public interface

  return {
    send, on, clear
  };
})();

