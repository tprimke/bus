'use strict';

module.exports = function () {

  // ----------------------------------------------------------------
  // Private properties and functions

  var emptyStore = function emptyStore() {
    return [];
  };

  var store = emptyStore();

  var newRecord = function newRecord(message, handler) {
    var type = arguments.length <= 2 || arguments[2] === undefined ? 'sync' : arguments[2];

    return {
      message: message, handler: handler, type: type
    };
  };

  var findHandlersMT = function findHandlersMT(msg, type) {
    return store.filter(function (r) {
      return r.message === msg && r.type === type;
    });
  };

  var findHandlersM = function findHandlersM(msg) {
    return store.filter(function (r) {
      return r.message === msg;
    });
  };

  var allType = function allType(msg, type) {
    return store.every(function (r) {
      return r.message === msg && r.type === type;
    });
  };

  var addSyncHandler = function addSyncHandler(msg, handler) {
    store.push(newRecord(msg, handler));
  };

  var addAsyncHandler = function addAsyncHandler(msg, handler) {
    store.push(newRecord(msg, handler, 'async'));
  };

  var makeValue = function makeValue(value) {
    return {
      type: 'value',
      value: value
    };
  };

  var makeArray = function makeArray(value) {
    return {
      type: 'array',
      value: value
    };
  };

  // ----------------------------------------------------------------
  // Public functions

  var send = function send(message) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var handlers = findHandlersM(message);
    if (handlers.length === 0) return;
    if (handlers.some(function (r) {
      return r.type === 'async';
    }) === true) throw new Error('Cannot synchronously send message to asynchronous handler.');

    var values = handlers.map(function (r) {
      return r.handler.apply(r, args);
    });
    if (values.length === 1) return makeValue(values[0]);
  };

  send.promise = function (msg) {
    for (var _len2 = arguments.length, args = Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
      args[_key2 - 1] = arguments[_key2];
    }

    var handlers = findHandlersM(msg);
    if (handlers.length === 0) {
      return;
    }

    // check the handler type
    if (handlers[0].type === 'sync') {
      var vals = send.apply(undefined, [msg].concat(args));
      return Promise.resolve(vals);
    }

    // handle asynchronous handlers
    var promises = handlers.map(function (r) {
      try {
        var val = r.handler.apply(r, args);
        return val;
      } catch (e) {
        return Promise.reject(e);
      }
    }).map(function (p) {
      if (p instanceof Promise) return p;
      return Promise.resolve(p);
    });
    return new Promise(function (resolve, reject) {
      Promise.all(promises).then(function (values) {
        if (values.length === 1) resolve(makeValue(values[0]));else {
          resolve(makeArray(values));
        }
      }).catch(function (err) {
        reject(err);
      });
    });
  };

  var on = function on(message, handler) {
    if (!allType(message, 'sync')) throw new Error('The ' + message + ' handler should be asynchronous.');
    addSyncHandler(message, handler);
  };

  on.promise = function (message, handler) {
    if (!allType(message, 'async')) throw new Error('The ' + message + ' handler should be synchronous.');
    addAsyncHandler(message, handler);
  };

  var clear = function clear() {
    for (var _len3 = arguments.length, handlers = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      handlers[_key3] = arguments[_key3];
    }

    if (handlers.length === 0) {
      store = emptyStore();
      return;
    }

    store = store.reduce(function (acc, elm) {
      if (handlers.indexOf(elm.handler) === -1) acc.push(elm);
      return acc;
    }, []);
  };

  // ----------------------------------------------------------------
  // Public interface

  return {
    send: send, on: on, clear: clear
  };
}();